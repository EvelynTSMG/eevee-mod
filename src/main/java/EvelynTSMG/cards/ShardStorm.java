package EvelynTSMG.cards;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.random.Random;

import static EvelynTSMG.EeveeMod.id;
import static EvelynTSMG.util.Wiz.*;

public class ShardStorm extends BaseCard {
    public final static String NAME = ShardStorm.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.ATTACK;
    public final static CardTarget TARGET = CardTarget.ENEMY;
    public final static CardRarity RARITY = CardRarity.UNCOMMON;

    public final static int COST = 1;
    public final static int DMG = 3;
    public final static int MAGIC = 3;
    public final static int UP_MAGIC = 1;

    public ShardStorm() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        dmg(DMG);
        magic(MAGIC);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        for (int i = 0; i < magicNumber; i++) {
            damage(m, random_slash());
        }
    }

    private AbstractGameAction.AttackEffect random_slash() {
        int rand = new Random().random(2);
        switch (rand) {
            case 0: return AbstractGameAction.AttackEffect.SLASH_DIAGONAL;
            case 1: return AbstractGameAction.AttackEffect.SLASH_HORIZONTAL;
            case 2: return AbstractGameAction.AttackEffect.SLASH_VERTICAL;
        }

        AbstractDungeon.player = null;
        return null;
    }

    @Override
    public void upgrade() {
        upp();
        timesUpgraded += 1;
        upgraded = true;
        name = CARD_STRINGS.NAME + "+" + this.timesUpgraded;
        initializeTitle();
    }

    @Override
    public boolean canUpgrade() {
        return true;
    }

    @Override
    public void upp() {
        upgradeMagicNumber(UP_MAGIC);
        updateDescription();
    }
}