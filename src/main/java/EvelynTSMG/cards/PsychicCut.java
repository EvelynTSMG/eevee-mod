package EvelynTSMG.cards;

import EvelynTSMG.powers.RegenPower;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static EvelynTSMG.EeveeMod.id;
import static EvelynTSMG.util.Wiz.*;

public class PsychicCut extends BaseCard {
    public final static String NAME = PsychicCut.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.ATTACK;
    public final static CardTarget TARGET = CardTarget.ENEMY;
    public final static CardRarity RARITY = CardRarity.UNCOMMON;

    public final static int COST = 1;
    public final static int DMG = 12;
    public final static int UP_DMG = 6;
    public final static int MAGIC = 4;

    public PsychicCut() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        dmg(DMG);
        magic(MAGIC);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        damage(m, AbstractGameAction.AttackEffect.SLASH_HORIZONTAL);
        apply_enemy(new RegenPower(m, 4));
    }

    @Override
    public void upp() {
        upgradeDamage(UP_DMG);
        updateDescription();
    }
}