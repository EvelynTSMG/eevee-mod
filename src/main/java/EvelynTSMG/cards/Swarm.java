package EvelynTSMG.cards;

import EvelynTSMG.powers.SwarmedPower;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static EvelynTSMG.EeveeMod.id;
import static EvelynTSMG.util.Wiz.*;

public class Swarm extends BaseCard {
    public final static String NAME = Swarm.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.ENEMY;
    public final static CardRarity RARITY = CardRarity.COMMON;

    public final static int COST = 1;
    public final static int MAGIC = 4;
    public final static int UP_MAGIC = 2;

    public Swarm() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        magic(MAGIC);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        apply_enemy(new SwarmedPower(m, magicNumber));
    }

    @Override
    public void upp() {
        upgradeMagicNumber(UP_MAGIC);
        updateDescription();
    }
}