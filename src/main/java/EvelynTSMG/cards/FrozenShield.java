package EvelynTSMG.cards;

import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static EvelynTSMG.EeveeMod.id;
import static EvelynTSMG.util.Wiz.*;

public class FrozenShield extends BaseCard {
    public final static String NAME = FrozenShield.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.BASIC;

    public final static int COST = 1;
    public final static int BLOCK = 6;
    public final static int UP_BLOCK = 2;
    public final static int MAGIC = 1;
    public final static int UP_MAGIC = 1;

    public FrozenShield() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        block(BLOCK);
        magic(MAGIC);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        block();
    }

    @Override
    public void upp() {
        upgradeBlock(UP_BLOCK);
        upgradeMagicNumber(UP_MAGIC);
        updateDescription();
    }
}