package EvelynTSMG.cards;

import basemod.helpers.CardTags;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static EvelynTSMG.EeveeMod.id;
import static EvelynTSMG.util.Wiz.add_tags;

public class Strike extends BaseCard {
    public final static String NAME = Strike.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.ATTACK;
    public final static CardTarget TARGET = CardTarget.ENEMY;
    public final static CardRarity RARITY = CardRarity.BASIC;

    public final static int COST = 1;
    public final static int DMG = 6;
    public final static int UP_DMG = 3;

    public Strike() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        dmg(DMG);
        add_tags(this, CardTags.STARTER_STRIKE, CardTags.STRIKE);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        damage(m, AbstractGameAction.AttackEffect.SLASH_DIAGONAL);
    }

    @Override
    public void upp() {
        upgradeDamage(UP_DMG);
    }
}