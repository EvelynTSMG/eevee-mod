package EvelynTSMG.cards;

import EvelynTSMG.powers.StasisSpurPower;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static EvelynTSMG.EeveeMod.id;
import static EvelynTSMG.util.Wiz.*;

public class StasisSpur extends BaseCard {
    public final static String NAME = StasisSpur.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.POWER;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.RARE;

    public final static int COST = 2;
    public final static int UP_COST = 1;
    public final static int MAGIC = 1;

    public StasisSpur() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        magic(MAGIC);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        apply_self(new StasisSpurPower(p, magicNumber));
    }

    @Override
    public void upp() {
        upgradeBaseCost(UP_COST);
        updateDescription();
    }
}