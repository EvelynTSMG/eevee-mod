package EvelynTSMG.cards;

import EvelynTSMG.actions.CompareAction;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.actions.common.GainBlockAction;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static EvelynTSMG.EeveeMod.id;
import static EvelynTSMG.util.Wiz.*;

public class MimicStrike extends BaseCard {
    public final static String NAME = MimicStrike.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.ATTACK;
    public final static CardTarget TARGET = CardTarget.ENEMY;
    public final static CardRarity RARITY = CardRarity.UNCOMMON;

    public final static int COST = 1;
    public final static int DMG = 7;
    public final static int UP_DMG = 4;

    public MimicStrike() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        dmg(DMG);
        add_tags(this, CardTags.STRIKE);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        atb(new CompareAction<>(
                new DamageAction(
                    m,
                    new DamageInfo(p, damage, DamageInfo.DamageType.NORMAL),
                    AbstractGameAction.AttackEffect.BLUNT_LIGHT),
                () -> m.currentBlock,
                (old_block) -> {
                    int block_lost = old_block - m.currentBlock;
                    if (block_lost > 0) {
                        att(new GainBlockAction(p, block_lost));
                    }
                }));
    }

    @Override
    public void upp() {
        upgradeDamage(UP_DMG);
        updateDescription();
    }
}