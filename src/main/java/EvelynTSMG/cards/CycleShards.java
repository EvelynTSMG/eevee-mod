package EvelynTSMG.cards;

import EvelynTSMG.actions.CompareAction;
import EvelynTSMG.actions.DrawFilteredCardAction;
import com.megacrit.cardcrawl.actions.common.DiscardAction;
import com.megacrit.cardcrawl.actions.common.DrawCardAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static EvelynTSMG.EeveeMod.id;
import static EvelynTSMG.util.Wiz.*;

public class CycleShards extends BaseCard {
    public final static String NAME = CycleShards.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.COMMON;

    public final static int COST = 0;
    public final static int MAGIC = 1;
    public final static int UP_MAGIC = 1;
    public final static int MAGIC2 = 1;

    public CycleShards() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        magic(MAGIC);
        magic2(MAGIC2);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        atb(new CompareAction<>(
                new DiscardAction(player(), player(), 1, false),
                () -> null,
                old -> {
                    CardType discarded_type = AbstractDungeon.handCardSelectScreen.selectedCards.group.get(0).type;
                    att(new DrawFilteredCardAction(c -> c.type == discarded_type, magicNumber));
                }));
    }

    @Override
    public void upp() {
        upgradeMagicNumber(UP_MAGIC);
        updateDescription();
    }
}