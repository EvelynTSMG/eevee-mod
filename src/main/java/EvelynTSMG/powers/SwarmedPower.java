package EvelynTSMG.powers;

import com.badlogic.gdx.math.MathUtils;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.actions.common.LoseHPAction;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;

import static EvelynTSMG.EeveeMod.id;
import static EvelynTSMG.util.Wiz.*;

public class SwarmedPower extends BasePower {
    public final static String NAME = SwarmedPower.class.getSimpleName();
    public final static PowerType TYPE = PowerType.DEBUFF;
    public final static boolean TURN_BASED = true;
    public static final int DEFAULT_AMOUNT = 1;

    // Default constructor for AutoAdd
    public SwarmedPower() {
        this(AbstractDungeon.player);
    }

    public SwarmedPower(AbstractCreature owner) {
        this(owner, DEFAULT_AMOUNT);
    }

    public SwarmedPower(AbstractCreature owner, int amount) {
        super(NAME, TYPE, TURN_BASED, owner, amount);
    }

    @Override
    public void atEndOfTurn(boolean isPlayer) {
        atb(new LoseHPAction(owner, owner, amount));
    }

    @Override
    public int onAttacked(DamageInfo info, int damageAmount) {
        if (info.owner != owner) {
            reducePower(MathUtils.ceilPositive((float) amount / 2f));
            updateDescription();
        }

        return super.onAttacked(info, damageAmount);
    }

    @Override
    public void updateDescription() {
        String raw_description = DESCRIPTIONS[owner == player() ? 0 : 1];
        description = String.format(raw_description, amount);
    }
}