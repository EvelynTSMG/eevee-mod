package EvelynTSMG.powers;

import com.megacrit.cardcrawl.actions.common.ReducePowerAction;
import com.megacrit.cardcrawl.actions.unique.RegenAction;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.localization.PowerStrings;

import static EvelynTSMG.EeveeMod.id;
import static EvelynTSMG.util.Wiz.*;

public class RegenPower extends BasePower {
    public final static String NAME = RegenPower.class.getSimpleName();
    public final static PowerType TYPE = PowerType.BUFF;
    public final static boolean TURN_BASED = true;
    public static final int DEFAULT_AMOUNT = 1;
    public static final PowerStrings STRINGS;

    // Default constructor for AutoAdd
    public RegenPower() {
        this(AbstractDungeon.player);
    }

    public RegenPower(AbstractCreature owner) {
        this(owner, DEFAULT_AMOUNT);
    }

    public RegenPower(AbstractCreature owner, int amount) {
        super(NAME, TYPE, TURN_BASED, owner, amount);
        name = STRINGS.NAME;
        updateDescription();
        loadRegion("regen");
    }

    @Override
    public void atEndOfTurn(boolean isPlayer) {
        flashWithoutSound();
        att(new RegenAction(owner, amount));
        reducePower(1);
    }

    @Override
    public void updateDescription() {
        description = STRINGS.DESCRIPTIONS[0] + amount + STRINGS.DESCRIPTIONS[1];
    }

    static {
        STRINGS = CardCrawlGame.languagePack.getPowerStrings("Regeneration");
    }
}