package EvelynTSMG.powers;

import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;

import static EvelynTSMG.EeveeMod.id;
import static EvelynTSMG.util.Wiz.*;

public class StasisSpurPower extends BasePower {
    public final static String NAME = StasisSpurPower.class.getSimpleName();
    public final static PowerType TYPE = PowerType.BUFF;
    public final static boolean TURN_BASED = false;
    public static final int DEFAULT_AMOUNT = 1;

    // Default constructor for AutoAdd
    public StasisSpurPower() {
        this(AbstractDungeon.player);
    }

    public StasisSpurPower(AbstractCreature owner) {
        this(owner, DEFAULT_AMOUNT);
    }

    public StasisSpurPower(AbstractCreature owner, int amount) {
        super(NAME, TYPE, TURN_BASED, owner, amount);
    }

    @Override
    public void atStartOfTurn() {
        //TODO: Make this power work when stasis is implemented
        super.atStartOfTurn();
    }

    @Override
    public void updateDescription() {
        description = String.format(DESCRIPTIONS[0], amount);
    }
}