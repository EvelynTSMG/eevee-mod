package EvelynTSMG.relics;

import EvelynTSMG.actions.PlayRandomCopyAction;
import com.megacrit.cardcrawl.actions.GameActionManager;
import com.megacrit.cardcrawl.cards.CardGroup;

import static EvelynTSMG.EeveeMod.id;
import static EvelynTSMG.util.Wiz.*;

public class DiamondShard extends BaseRelic {
    public final static String NAME = DiamondShard.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static RelicTier TIER = RelicTier.STARTER;
    public final static LandingSound SOUND = LandingSound.CLINK;

    public DiamondShard() {
        super(NAME, TIER, SOUND);
    }

    @Override
    public void atTurnStartPostDraw() {
        if (GameActionManager.turn > 1) return;

        atb(new PlayRandomCopyAction(
                card -> {
                    flash();
                    flash_above_player();
                },
                CardGroup.CardGroupType.HAND));
    }
}