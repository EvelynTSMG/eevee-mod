package EvelynTSMG.actions;

import EvelynTSMG.EeveeMod;
import EvelynTSMG.relics.BaseRelic;
import basemod.cardmods.ExhaustMod;
import basemod.helpers.CardModifierManager;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.utility.NewQueueCardAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.CardGroup;

import java.util.ArrayList;
import java.util.Optional;
import java.util.function.Consumer;

import static EvelynTSMG.util.Wiz.atb;
import static EvelynTSMG.util.Wiz.player;
import static EvelynTSMG.util.Wiz.random_card_from;
import static EvelynTSMG.util.Wiz.random_item;

@SuppressWarnings("OptionalUsedAsFieldOrParameterType")
public class PlayRandomCopyAction extends AbstractGameAction {
    private final CardGroup.CardGroupType[] from;
    private final Optional<Consumer<AbstractCard>> on_success;
    private final Optional<Runnable> on_failure;

    public PlayRandomCopyAction(CardGroup.CardGroupType... from) {
        this(Optional.empty(), Optional.empty(), from);
    }

    public PlayRandomCopyAction(Consumer<AbstractCard> on_success, CardGroup.CardGroupType... from) {
        this(Optional.ofNullable(on_success), Optional.empty(), from);
    }

    public PlayRandomCopyAction(Runnable on_failure, CardGroup.CardGroupType... from) {
        this(Optional.empty(), Optional.ofNullable(on_failure), from);
    }

    public PlayRandomCopyAction(Consumer<AbstractCard> on_success, Runnable on_failure, CardGroup.CardGroupType... from) {
        this(Optional.ofNullable(on_success), Optional.ofNullable(on_failure), from);
    }

    public PlayRandomCopyAction(
            Optional<Consumer<AbstractCard>> on_success,
            Optional<Runnable> on_failure,
            CardGroup.CardGroupType... from) {
        this.from = from;
        this.on_success = on_success;
        this.on_failure = on_failure;
    }

    @Override
    public void update() {
        Optional<AbstractCard> random_card = random_card_from(c -> c.cost != -2, from);
        if (random_card.isPresent()) {
            AbstractCard copy = random_card.get().makeCopy();
            CardModifierManager.addModifier(copy, new ExhaustMod());
            on_success.ifPresent(a -> a.accept(copy));

            atb(new NewQueueCardAction(copy, true, true, true));
        } else {
            on_failure.ifPresent(Runnable::run);
        }

        isDone = true;
    }
}
